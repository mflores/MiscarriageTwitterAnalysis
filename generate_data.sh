# For this to run, have pkl files of tweets (df_tweets_100.pkl, df_tweets_1000.pkl, df_tweets_10000.pkl, df_tweets_all.pkl) 
# in data/pickle folder. These will be the tweets that we want to run the full analysis below on.

#!/bin/bash

# Set the Python version and scripts directory
PYTHON_VERSION=3.10
SCRIPT_DIR="./scripts/"

# Change directory to the scripts directory
cd $SCRIPT_DIR

echo "Merging tweets and author info data"
python$PYTHON_VERSION tweets_with_author_info.py

echo "Performing initial cleaning of data"
python$PYTHON_VERSION initial_cleaning.py

echo "Generating sentiment data"
python$PYTHON_VERSION sentiment_data.py

echo "Generating topic model data"
python$PYTHON_VERSION topic_model_data.py

#echo "Generating author data"
#python$PYTHON_VERSION ./data/scripts/authors_data.py

echo "Merging data into the final data set"
python$PYTHON_VERSION merge_datasets.py

echo "Complete! Look for the final data set called final_df_tweets.pkl in the data folder and run the analysis notebook!"
