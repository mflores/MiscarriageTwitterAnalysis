import os
import json
import yaml
import pandas as pd
import re

config = yaml.safe_load(open("../config/config.yaml"))

current_directory = "../"
sample_size = config["datasets"]["sample_size"]
pickle_folder = config["datasets"]["pickle_folder"]

data_filename = config['datasets']['tweets_authors_filename']
data_file = f"{data_filename}_{sample_size}.pkl"
data_location = os.path.join(current_directory, pickle_folder, data_file)
df_tweets = pd.read_pickle(data_location)

print("Before cleaning: ", len(df_tweets))

# Remove tweets that are repetitive tweets
# Read JSON data from a file
pair_results_filename = config['datasets']['pair_results_filename']
pair_results_filename = os.path.join(current_directory, pickle_folder, pair_results_filename)

# Remove tweets who we don't have author data on
# Remove tweets where there is no information about the counts
df_tweets = df_tweets[~df_tweets['listed_count'].isna() | ~df_tweets['followers_count'].isna() | ~df_tweets['following_count'].isna() | ~df_tweets['tweet_count'].isna()]
print("Removed tweets we have no author data on: ", len(df_tweets))

# Remove tweets where the followers, followees, tweets counts is zero
#df_tweets = df_tweets[~(df_tweets['followers_count'] == 0) | ~(df_tweets['following_count'] == 0) | ~(df_tweets['tweet_count'] == 0)]
df_tweets = df_tweets[~((df_tweets['followers_count'] == 0) | (df_tweets['following_count'] == 0) | (df_tweets['tweet_count'] == 0))]
print("Removed tweets that have zero followers, following, or tweets: ", len(df_tweets))

# Delete the events that = 0 (they were outside the labels)
df_tweets = df_tweets[df_tweets['event'] != '0']
print("Removed tweets where event is 0: ", len(df_tweets))

# Remove tweets by government and business
df_tweets = df_tweets[df_tweets['verified_type'] != 'business']
df_tweets = df_tweets[df_tweets['verified_type'] != 'government']
print("Removed tweets whose authors are business or government: ", len(df_tweets))

# Remove tweets by bots
pattern = r'\bbot\b'
# Check if the word "bot" is in the description column
is_bot = df_tweets['description'].str.contains(pattern, case=False, regex=True)
# Create a new column to indicate if the user is a bot
df_tweets['is_bot'] = is_bot
df_tweets = df_tweets[df_tweets['is_bot'] == False]
df_tweets = df_tweets.drop('is_bot', axis=1)
print("Removed tweets that could be bots based on description: ", len(df_tweets))

# Removing tweets that only have keywords in the URL
terms = ['abortion', 'miscarriage', 'feticide', 'foeticide', 'aborticide', 'stillbirth',
             'early pregnancy loss', 'fetal homicide', 'child destruction']

# Create a pattern to match any of the specified terms
pattern = r'\b(?:{})\b'.format('|'.join(terms))

# Filter the DataFrame based on the pattern
df_tweets = df_tweets[df_tweets['cleaned_text'].str.contains(pattern, case=False, regex=True)]
print("Removed tweets that only have keywords in URL: ", len(df_tweets))

# Paraphrase mining cleaning
with open(pair_results_filename, 'r') as json_file:
    json_data = json_file.read()
# Parse JSON data into a dictionary
parsed_list = json.loads(json_data)

# Create a new DataFrame to store the aggregated counts
aggregated_df = pd.DataFrame(columns=['id', 'retweet_count_agg', 'like_count_agg', 'quote_count_agg', 'reply_count_agg'])

list_all_except_first = list()
for id_group in parsed_list:
    all_except_first = df_tweets[df_tweets['id'].isin(id_group)][['id', 't_created_at']].sort_values(by='t_created_at')['id'].values
    try:  # As the cleaning is after previous cleaning sometimes the list is empty as the tweets already being removed
        list_all_except_first.extend(all_except_first[1:])
        only_first_id = all_except_first[0]
        # Filter rows with IDs from the current group
        group_df = df_tweets[df_tweets['id'].isin(id_group)]
        
        # Sum the counts for each group
        sum_retweet = group_df['public_metrics.retweet_count'].sum()
        sum_reply = group_df['public_metrics.reply_count'].sum()
        sum_like = group_df['public_metrics.like_count'].sum()
        sum_quote = group_df['public_metrics.quote_count'].sum()
        id_to_use = only_first_id
        
        # Create a new row for the aggregated counts
        new_row = {
            'id': id_to_use,
            'retweet_count_agg': sum_retweet,
            'like_count_agg': sum_like,
            'quote_count_agg': sum_quote,
            'reply_count_agg': sum_reply
        }
        new_row_df = pd.DataFrame([new_row])
        aggregated_df = pd.concat([aggregated_df, new_row_df], ignore_index=True)
    except IndexError:
        pass

# Update the original DataFrame with aggregated counts
df_tweets = pd.merge(df_tweets, aggregated_df, on='id', how='left')

# Fill missing values with original counts
df_tweets['retweet_count_agg'].fillna(df_tweets['public_metrics.retweet_count'], inplace=True)
df_tweets['like_count_agg'].fillna(df_tweets['public_metrics.like_count'], inplace=True)
df_tweets['quote_count_agg'].fillna(df_tweets['public_metrics.quote_count'], inplace=True)
df_tweets['reply_count_agg'].fillna(df_tweets['public_metrics.reply_count'], inplace=True)

# Add total metric agg
df_tweets['total_count_agg'] = df_tweets['retweet_count_agg'] + df_tweets['like_count_agg'] + df_tweets['quote_count_agg'] + df_tweets['reply_count_agg']

# Remove all the tweets that are similar but older than the original 
df_tweets = df_tweets[~df_tweets['id'].isin(list_all_except_first)]
print("Removed tweets that are similar with paraphrase mining results: ", len(df_tweets))

# Remove flagged authors 
authors_to_remove = ['Inspiredtvgh', 'zhouzhoujyk', 'youkayoyo10059', 'littlebytenews', 'usasharenews', 'motherslost', 'CAdabag', 'jeguru1', 'JohnRosePutnam', 'Brospar2022', 'VCPNewz', 'equalearth', 'theliveusa', 'OedipaB']
df_tweets = df_tweets[~df_tweets['author.username'].isin(authors_to_remove)]
print("Removed tweets whose authors we flagged: ", len(df_tweets))

# Remove tweets by news
news_data_file = "../data/twitter_files/news_outlets-accounts.csv"
news_outlets = pd.read_csv(news_data_file)

news_id_list = news_outlets['Uid'].tolist()

# Uncomment to save list of news outlets found in the dataset whose tweets are removed
# import numpy as np
# temp = df_tweets[df_tweets['author_id'].isin(news_id_list)]
# news_outlets_list = temp['author.username'].unique()
# news_outlets_list = pd.DataFrame(news_outlets_list, columns=['News'])
# news_outlets_list.to_csv("news_outlets.csv", index=False)

df_tweets = df_tweets[~df_tweets['author_id'].isin(news_id_list)]
print("Removed tweets by news sources: ", len(df_tweets))

# Remove URLs and MENTIONS from cleaned_text
# Define the pattern to remove words starting with '__'
pattern_to_remove = r'\b__\w+\b'

# Compile the regular expression pattern
compiled_pattern = re.compile(pattern_to_remove)

def docs_cleaning(doc, compiled_pattern):
    cleaned_doc = compiled_pattern.sub('', doc)
    return cleaned_doc

df_tweets['cleaned_text'] = df_tweets['cleaned_text'].apply(lambda x: docs_cleaning(x, compiled_pattern))


# Export just the tweets dataset for the topic model
cleaned_data_filename = config["datasets"]["cleaned_data_filename"]
cleaned_data_file = f"{cleaned_data_filename}_{sample_size}.pkl"
cleaned_data_location = os.path.join(current_directory, pickle_folder, cleaned_data_file)
df_tweets.to_pickle(cleaned_data_location)


