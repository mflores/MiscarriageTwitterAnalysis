# %%
import pandas as pd
import yaml
import os
import numpy as np

# Assuming all tweet datasets include the unique ID of the tweet
# First, we merge all of the tweet datasets together based on unique id
# Then, we go through the dataset and based on author id of each tweet, add author data to each tweet 

config = yaml.safe_load(open("../config/config.yaml"))
current_directory = "../"

data_folder = config['datasets']['pickle_folder']
sample_size = config['datasets']['sample_size']

sentiment_filename = config['datasets']['sentiment_filename']
sentiment_file = f"{sentiment_filename}_{sample_size}.pkl"
sentiment_location = os.path.join(current_directory, data_folder, sentiment_file)

depression_filename = config['datasets']['depression_filename']
depression_file = f"{depression_filename}_{sample_size}.pkl"
depression_location = os.path.join(current_directory, data_folder, depression_file)

num_topics = config['bertopic']['num_topics']
topics_filename = config['datasets']['topics_filename']
topics_file = f"{topics_filename}_{num_topics}_{sample_size}.pkl"
topics_location = os.path.join(current_directory, data_folder, topics_file)

sample_size = config['datasets']['sample_size']
tweets_filename = config['datasets']['cleaned_data_filename']
tweets_file = f"{tweets_filename}_{sample_size}.pkl"
tweets_location = os.path.join(current_directory, data_folder, tweets_file)
 
df_sentiment = pd.read_pickle(sentiment_location)
df_depression = pd.read_pickle(depression_location)
df_topics = pd.read_pickle(topics_location)
df_tweets = pd.read_pickle(tweets_location)

df_depression = df_depression.drop(['cleaned_text'], axis=1)
# add binary depression column
df_depression['has_depression'] = df_depression['overall depression'].isin(['moderate depression', 'severe depression'])
#df_topics = df_topics.drop_duplicates(subset='id')

df_tweets = pd.merge(df_tweets, df_sentiment, on='id')
df_tweets = pd.merge(df_tweets, df_depression, on='id')
df_tweets = pd.merge(df_tweets, df_topics, on='id')

# CLEANING FOR FINAL DATASET

#Rename the rows in the event column
event_labels = {'Miscarriage 21': 'WPILRD 2021',
                    'Miscarriage 22': 'WPILRD 2022',
                    'Abortion 21': 'ISAD 2021',
                    'Abortion 22': 'ISAD 2022'}

df_tweets['event'] = df_tweets['event'].replace(event_labels)

#Rename the rows in the gender column
gender_labels = {'female':'Woman',
                     'male':'Man',
                     'neutral':'Unclassified'}
    
df_tweets['gender'] = df_tweets['gender'].replace(gender_labels)
df_tweets['gender'] = np.where(df_tweets['verified_type'] == 'business', 'Unclassified', df_tweets['gender'])

#Rename the rows in the verified type column
user_labels = {'none':'Normal',
                   'blue':'Blue check',
                   'business':'Business',
                   'government':'Government'}
    
df_tweets['verified_type'] = df_tweets['verified_type'].replace(user_labels)

#Rename the rows in the emotion column
emotion_labels = {'anger':'Anger',
                      'disgust':'Disgust',
                      'fear':'Fear',
                      'joy':'Joy',
                      'neutral':'Neutral',
                      'sadness':'Sadness',
                      'surprise':'Surprise'
                      }
    
df_tweets['top emotion'] = df_tweets['top emotion'].replace(emotion_labels)


df_tweets.loc[:, 't_created_at'] = pd.to_datetime(df_tweets['t_created_at'], format='%d-%m-%y')
    
#Create the ["year"] column for graphs
df_tweets['year'] = pd.DatetimeIndex(df_tweets['t_created_at']).year

#Create the ["date"] column for graphs
df_tweets['date'] = pd.DatetimeIndex(df_tweets['t_created_at']).date

#Create the ["hour"] column for graphs
df_tweets['hour'] = pd.DatetimeIndex(df_tweets['t_created_at']).hour

# Apply the function and create a new column
df_tweets['t_event_noyear'] = np.where(df_tweets['event'].isin(['WPILRD 2021','WPILRD 2022']), 'WPILRD', 'ISAD')

to_rename = {
        'id': 't_id',
        'text': 't_text',
        'author_id': 'a_id',
        'author.name': 'a_name',
        'public_metrics.retweet_count': 't_retweet_count',
        'public_metrics.reply_count': 't_reply_count',
        'public_metrics.like_count': 't_like_count',
        'public_metrics.quote_count': 't_quote_count',
        'retweet_count_agg': 't_retweet_count_agg',
        'reply_count_agg': 't_reply_count_agg',
        'like_count_agg': 't_like_count_agg',
        'quote_count_agg': 't_quote_count_agg',
        'followers_count': 'a_followers_count',
        'following_count': 'a_following_count',
        'tweet_count': 'a_tweet_count',
        'listed_count': 'a_listed_count',
        'gender': 'a_gender',
        'total_metrics': 't_total_count',
        'total_count_agg': 't_total_count_agg',
        'Topic': 't_topic',
        'rbta_compound': 't_polarity',
        'tb_subjectivity': 't_subjectivity',
        'verified_type': 'a_verified_type',
        'event': 't_event',
        'year': 't_year',
        'date': 't_date',
        'hour': 't_hour',
        'overall depression': 't_depression',
        'Label': 't_topic_label'
    }
df_tweets = df_tweets.rename(columns=to_rename)
config = yaml.safe_load(open("../config/config.yaml"))
final_data_filename = config['datasets']['final_data_filename']
final_data_file = f"{final_data_filename}_{sample_size}.pkl"
final_data_location = os.path.join(current_directory, data_folder, final_data_file)
df_tweets.to_pickle(final_data_location)
