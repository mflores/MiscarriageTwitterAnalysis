import pandas as pd
import os
import yaml

config = yaml.safe_load(open(f"../config/config.yaml"))
current_directory = '../'

# load df_tweets_cleaned
sample_size = config['datasets']['sample_size']
data_folder = config['datasets']['pickle_folder']
data_filename = config['datasets']['cleaned_data_filename']
data = f"{data_filename}_{sample_size}.pkl"
data_location = os.path.join(current_directory, data_folder, data)
df_tweets_cleaned = pd.read_pickle(data_location)

# load current final_dataset
final_data_folder = config['datasets']['pickle_folder']
final_data_filename = config['datasets']['final_data_filename']
final_data_file = f"{final_data_filename}_{sample_size}.pkl"
final_data_location = os.path.join(current_directory, final_data_folder, final_data_file)
df = pd.read_pickle(final_data_location)

# get subset
id_list = df_tweets_cleaned['id'].tolist()
df = df[df['t_id'].isin(id_list)]

# export new final_dataset
df.to_pickle(final_data_location)