import os
import re
import logging
import json
import yaml
from tqdm import tqdm
import pandas as pd
import pickle
from sentence_transformers import SentenceTransformer, util
import torch

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO
)

config = yaml.safe_load(open("./config/config.yaml"))
sample_size = config["datasets"]["sample_size"]
pickle_folder = config["datasets"]["pickle_folder"]
final_data_filename = config["datasets"]["tweets_filename"]
final_data_file = f"{final_data_filename}_{sample_size}.pkl"
final_data_location = os.path.join(pickle_folder, final_data_file)

embeddings_filename = config['datasets']['embeddings_filename']
embeddings_filename = os.path.join(pickle_folder, embeddings_filename)
distances_filename = config['datasets']['distances_filename']
distances_filename = os.path.join(pickle_folder, distances_filename)
pair_results_filename = config['datasets']['pair_results_filename']
pair_results_filename = os.path.join(pickle_folder, pair_results_filename)


model_name = config["paraphrase"]["model_name"]
reload_embeddings = config["paraphrase"]["reload_embeddings"]
reload_distances = config["paraphrase"]["reload_distances"]
threshold = config["paraphrase"]["threshold"]

logging.info(f"load the model: {model_name}")
model = SentenceTransformer(model_name)

logging.info("Load the dataframe")
df_raw = pd.read_pickle(final_data_location)

# Remove the punctuation hashtags and the __URL__ __MENTIONS__ to match more similar sentence with only a few modifications
patterns_to_remove = [
        r'__URL__',
        r'__MENTION__',
        r'[.,!?;:]',
        r'\s*#\w+\s*'
    ]

# Combine all patterns into a single pattern using the | (OR) operator
combined_pattern = '|'.join(patterns_to_remove)

# Compile the regular expression pattern
compiled_pattern = re.compile(combined_pattern)

def last_cleaning_sentence(sentence, compiled_pattern):
    cleaned_string = compiled_pattern.sub('', sentence)
    return cleaned_string.lower()

# Single list of sentences
sentences = df_raw['cleaned_text']
sentences = [last_cleaning_sentence(i, compiled_pattern) for i in sentences]
# Create a id column in numpy array for fastest iteration during the pair finding
id_col = df_raw["id"].values



# Compute embeddings
if reload_embeddings is True:
    with open(embeddings_filename, "rb") as pkl:
        embeddings = pickle.load(pkl)
else:
    logging.info("Encoding embeddings")
    embeddings = model.encode(sentences, convert_to_tensor=True)
    logging.info("Saving embeddings")
    with open(embeddings_filename, "wb") as pkl:
        pickle.dump(embeddings, pkl)

logging.info("Distance metrics")
if reload_distances is True:
    logging.info("Loading")
    with open(distances_filename, "rb") as pkl:
        distance = pickle.load(pkl)
else:
    logging.info("Calculating distance metrics")
    # Compute cosine-similarities for each sentence with each other sentence
    distance = util.cos_sim(embeddings, embeddings)
    # distance = sklearn.metrics.pairwise.pairwise_distances(embeddings, metric='cosine')
    distance.fill_diagonal_(0.0)
    logging.info("Saving distance metrics")

    # Save the distance array to a pickle file
    with open(distances_filename, "wb") as f:
        pickle.dump(distance, f)


# Define the threshold

# Find pairs above the threshold sequentially
logging.info("Getting the list of indices")
logging.info("Zeroing the diagonal")
indices = torch.nonzero(distance >= threshold)

logging.info("Convert the tensor into numpy")
# Convert TensorFlow tensor to NumPy array for printing
indices_np = indices.numpy()

logging.info("Starting to reorganise the pairs")
list_set = []
total_pairs = len(indices_np)
with tqdm(total=total_pairs, desc="Grouping pairs") as pbar:
    for i, j in indices_np:
        # Get the ids from the indice index
        t_row = str(id_col[i])
        t_col = str(id_col[j])
        # Need to create a temp list of set to be merged together
        # it is to avoid duplicate items with partial set
        temp_list_set = list()
        for my_set in list_set:
            # If any of the t_row or t_col presents because 
            # We don't know at that point which one has been already Parsed
            if t_row in my_set or t_col in my_set:
                # Create the temp list of set to be merged
                temp_list_set.append(my_set)
                # Removing the current set from the list as it will be
                # Added back after being merged with other potential set
                list_set.remove(my_set)
        # Add the current t_row and t_col to the list of temp sets
        temp_list_set.append({t_row, t_col})
        # Merge all the sets presents into a single one
        to_add_set = set().union(*temp_list_set) 
        # Add the set back to the list_set
        list_set.append(to_add_set)

        pbar.update(1)

logging.info("Recording the pairs in json file")
list_of_lists = [list(my_set) for my_set in list_set]
# Save the dictionary as a JSON file
with open(pair_results_filename, "w") as file:
    json.dump(list_of_lists, file)
