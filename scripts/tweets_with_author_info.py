import os
import yaml
import pandas as pd

config = yaml.safe_load(open("../config/config.yaml"))

current_directory = "../"
sample_size = config["datasets"]["sample_size"]
pickle_folder = config["datasets"]["pickle_folder"]
tweets_data_filename = config["datasets"]["tweets_filename"]
tweets_data_file = f"{tweets_data_filename}_{sample_size}.pkl"
tweets_data_location = os.path.join(current_directory, pickle_folder, tweets_data_file)

df_tweets = pd.read_pickle(tweets_data_location)

# Load author dataset
data_folder = config['datasets']['pickle_folder']
authors_filename = config['datasets']['authors_filename']
authors_file = f"{authors_filename}_{sample_size}.pkl"
authors_location = os.path.join(current_directory, data_folder, authors_file)
df_authors = pd.read_pickle(authors_location)

# Rename and drop necessary columns
df_authors.rename(columns = {'id':'author_id'}, inplace = True)
df_authors.rename(columns = {'created_at':'a_created_at'}, inplace = True)
df_tweets.rename(columns = {'created_at':'t_created_at'}, inplace = True)
df_tweets = df_tweets.drop(['author.id'], axis=1)

# Get list of column names in author dataset, create empty columns with same names in tweet dataset
author_info = df_authors.columns
author_info = author_info.drop(['author_id', 'username'])
author_info = author_info.tolist()

# Add empty columns onto tweets df
df_tweets = df_tweets.reindex(columns = df_tweets.columns.tolist() + author_info)

list_authors = df_authors['author_id'].tolist()

# Make author_id field integers so that we can sort them, author_id in the author df is already an integer
df_tweets['author_id'] = pd.to_numeric(df_tweets['author_id'])

# Sort tweets df and authors df by author id
df_tweets = df_tweets.sort_values(by='author_id', ascending=True).reset_index()
df_authors = df_authors.sort_values(by='author_id', ascending=True).reset_index()

def populate_tweet_author_info(tweet_index, author_index):
    for field in author_info:
        df_tweets[field][tweet_index] = df_authors[field][author_index]
        #df_tweets.loc[tweet_index, field] = df_authors.loc[author_index, field]

author_index = 0
tweet_index = 0

while(author_index < len(df_authors)-1):
    while(df_tweets['author_id'][tweet_index] == df_authors['author_id'][author_index]):
        populate_tweet_author_info(tweet_index, author_index)
        tweet_index += 1
    # Take care of case where we might have tweet with no author
    while((df_tweets['author_id'][tweet_index] != df_authors['author_id'][author_index]) and 
          (df_tweets['author_id'][tweet_index] > df_authors['author_id'][author_index])):
        author_index += 1
    # If the author for this tweet isn't in the author df, move on to next tweet
    if(df_tweets['author_id'][tweet_index] < df_authors['author_id'][author_index]):
        tweet_index += 1

config = yaml.safe_load(open("../config/config.yaml"))
data_filename = config['datasets']['tweets_authors_filename']
data_file = f"{data_filename}_{sample_size}.pkl"
data_location = os.path.join(current_directory, pickle_folder, data_file)
df_tweets.to_pickle(data_location)