# %%
import sys
import yaml
#from transformers_helpers import load_model, load_embeddings, save_embeddings
import os
from bertopic import BERTopic
from umap import UMAP
from hdbscan import HDBSCAN
import json
from sklearn.feature_extraction import text
from sklearn.feature_extraction.text import CountVectorizer
from bertopic.vectorizers import ClassTfidfTransformer
import pandas as pd
import yaml
import os
import re
from nltk import word_tokenize
from nltk.stem import WordNetLemmatizer

class LemmaTokenizer:
    def __init__(self):
        self.wnl = WordNetLemmatizer()
    def __call__(self, doc):
        return [self.wnl.lemmatize(t) for t in word_tokenize(doc)]

# %%
config = yaml.safe_load(open(f"../config/config.yaml"))

current_directory = '../'

sample_size = config['datasets']['sample_size']
data_folder = config['datasets']['pickle_folder']
data_filename = config['datasets']['cleaned_data_filename']
data = f"{data_filename}_{sample_size}.pkl"
data_location = os.path.join(current_directory, data_folder, data)

df = pd.read_pickle(data_location)
docs = df['cleaned_text'].tolist()

num_topics = config['bertopic']['num_topics']
min_df = config['bertopic']['min_df']

topics_data_filename = config['datasets']['topics_filename']
topics_data = f"{topics_data_filename}_{num_topics}_{sample_size}.pkl"
topics_data_location = os.path.join(current_directory, data_folder, topics_data)
df_topics = pd.read_pickle(topics_data_location)

model = config['bertopic']['bertopic_model']
model_name = f"{model}_{num_topics}"
#model_name = 'bertopic_model' # this is the default model
model_folder = config['bertopic']['bertopic_folder']
model_location = os.path.join(current_directory, model_folder, model_name)
topic_model = BERTopic.load(model_location)

# %%

# Process the text even more to use for what is considered in the topic labels (exclude hashtags and emojis)
def preprocess_text(text):
    # Remove hashtags
    text = re.sub(r'#\w+', '', text)
    # Remove emojis
    text = re.sub(r'__\w+', '', text)
    # Convert to lowercase
    text = text.lower()
    return text

# Preprocess documents
preprocessed_docs_for_labels = [preprocess_text(doc) for doc in docs]

# %%
my_stopwords=list(text.ENGLISH_STOP_WORDS)
vectorizer_model = CountVectorizer(stop_words=my_stopwords, tokenizer=LemmaTokenizer(), min_df=min_df)
ctfidf_model = ClassTfidfTransformer(reduce_frequent_words=True)

topic_model.update_topics(preprocessed_docs_for_labels, vectorizer_model=vectorizer_model, ctfidf_model=ctfidf_model)

# %%
new_labels = topic_model.generate_topic_labels(nr_words=10) 
new_labels = pd.DataFrame(new_labels)
new_labels['Topic'] = new_labels.index - 1
new_labels.rename(columns = {0:'Label'}, inplace = True)
df_topics.rename(columns = {'Top_n_words':'old_labels'}, inplace = True)
df_topics = pd.merge(df_topics, new_labels, on='Topic')
df_topics = df_topics.drop('old_labels', axis=1)
df_topics = df_topics.drop('Name', axis=1)
df_topics = df_topics.drop('Representative_document', axis=1)

# %%
# Save bertopic model with updated labels
config = yaml.safe_load(open(f"../config/config.yaml"))
model_location = os.path.join(current_directory, model_folder, model_name)
topic_model.save(model_location)

# Save topics dataframe
# Save model as bertopic_model_updated for now so that we can still use 
# the original file in the analysis until we fix problem with loading updated model
# topics_filename = config['datasets']['topics_filename']
# topics_file = f"{topics_filename}_{sample_size}.pkl"
topics_data_location = os.path.join(current_directory, data_folder, topics_data)
df_topics.to_pickle(topics_data_location)


