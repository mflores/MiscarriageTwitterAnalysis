# %% [markdown]
# # Sentiment Analysis

# %%
import pandas as pd

#Packages for text cleaning 
import re
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

#Sentiment analysis
import nltk
nltk.download('vader_lexicon')
from nltk.sentiment import SentimentIntensityAnalyzer
from textblob import TextBlob
from transformers import AutoModelForSequenceClassification
from transformers import TFAutoModelForSequenceClassification
from transformers import AutoTokenizer, AutoConfig
from transformers import pipeline
import numpy as np
from scipy.special import softmax
import torch
import flax
import tensorflow
import yaml
import os

# %%
config = yaml.safe_load(open(f"../config/config.yaml"))

current_directory = '../'

sample_size = config['datasets']['sample_size']
data_folder = config['datasets']['pickle_folder']
data_filename = config['datasets']['cleaned_data_filename']
data = f"{data_filename}_{sample_size}.pkl"
data_location = os.path.join(current_directory, data_folder, data)

df = pd.read_pickle(data_location)

# %%
# This is the smaller dataframe that columns with sentiment data will be added to
df_sentiment = df[['id','text']].copy()

# %%
# Text Cleaning

def remove_extra_spaces(sentence):
    sentence = sentence.replace(r'\n',' ', regex=True) 
    sentence = sentence.replace(r'/\|/g',' ', regex=True) 
    return sentence

def lowercase_text(sentence):
    return sentence.lower()

def remove_usernames(sentence):
    sentence = re.sub(r'@\w+',' ', sentence)
    return sentence

def remove_hashtags(sentence):
    sentence = re.sub(r"#\w+",' ', sentence)
    return sentence

def remove_links(sentence):
    sentence = re.sub(r'\bhttps?://[^\s]*',' ', sentence)
    return sentence

def remove_numbers(sentence):
    sentence = re.sub(r'\d+',' ', sentence)
    return sentence

def remove_punctuation_and_spaces(sentence):
    sentence = re.sub(r'&', ' ', sentence) # Specific for &
    sentence = re.sub(r'[^\w\s]',' ', sentence)
    sentence = ' '.join(sentence.split())
    return sentence

def remove_1letter_words(sentence):
    sentence = re.sub(r'\b\w{1}\b', ' ', sentence)
    return sentence

def tokenize(sentence):
    list_of_words = sentence.split(" ")
    return list_of_words

def remove_stopwords(list_of_words):
    # Getting English stopwords
    stop_words = set(stopwords.words('english'))

    filtered_words = []
    for word in list_of_words: 
        if word not in stop_words: 
            filtered_words.append(word) 

    return filtered_words

def lemmatize(list_of_words):
    # Getting English lemmatizer
    wordnet_lemmatizer = WordNetLemmatizer()
    
    lemma_words = []
    for w in list_of_words:
        word1 = wordnet_lemmatizer.lemmatize(w, pos = "n") # Names
        word2 = wordnet_lemmatizer.lemmatize(word1, pos = "v") # Verbs
        word3 = wordnet_lemmatizer.lemmatize(word2, pos = "a") # Adjectives
        lemma_words.append(word3)

    return lemma_words

# Function to apply all text tranformations
def text_cleaning(df):
    '''
    Given a pandas dataframe with tweets returns a list of clean tweet texts
    @input: pandas dataframe with tweets
    @output: list of clean tweets (same order as in df)
    '''
    lists_of_text = []

    for text in df['text']:
        sentence = str(text)

        # Text transformations
        sentence = lowercase_text(sentence) # All in lowercase
       # sentence = remove_extra_spaces(sentence)

        sentence = remove_links(sentence) # Remove links
        sentence = remove_hashtags(sentence) # Remove Hashtags
        sentence = remove_usernames(sentence) # Remove usernames
        sentence = remove_numbers(sentence) # Remove numbers
        sentence = remove_punctuation_and_spaces(sentence) # Remove punctuation signs and multiple spaces
        sentence = remove_1letter_words(sentence) # Remove words with len=1
        sentence = remove_punctuation_and_spaces(sentence)
        
        list_of_words = tokenize(sentence) # Tokenization
        list_of_words = remove_stopwords(list_of_words) # Remove stopwords
        list_of_words = lemmatize(list_of_words) # Lemmatization
        
        # Converting list of words to text again
        lists_of_text.append(' '.join(list_of_words))

    return lists_of_text

# %%
df_sentiment['clean text'] = text_cleaning(df_sentiment)

# %% [markdown]
# ## Polarity (and Subjectivity)

# %%
# SIA polarity

def sentiment_analysis_polarity_NLTK(df):
    '''
    Given a dataframe with tweets, it adds 4 new columns to express sentiment polarity:
    negative score, positive score, neutral score and compound score
    '''

    sia = SentimentIntensityAnalyzer()

    tweets = df_sentiment['clean text']

    negative_values = []
    neutral_values = []
    positive_values = []
    compound_values = []

    for tweet in tweets:
        polarity_dict = sia.polarity_scores(tweet)
        
        negative_values.append(polarity_dict['neg'])
        neutral_values.append(polarity_dict['neu'])
        positive_values.append(polarity_dict['pos'])
        compound_values.append(polarity_dict['compound'])

    df['sia_negative'] = negative_values
    df['sia_positive'] = positive_values
    df['sia_neutral'] = neutral_values
    df['sia_compound'] = compound_values
    
    return df

df_sentiment = sentiment_analysis_polarity_NLTK(df_sentiment)

# %%
# TextBlob polarity and subjectivity

def subjectivity(text):
    return TextBlob(text).sentiment.subjectivity

#Function for general polarity
def polarity(text):
    return TextBlob(text).sentiment.polarity

df_sentiment['clean text'] = df_sentiment['clean text'].astype(str)
df_sentiment['tb_polarity'] = df_sentiment['clean text'].apply(polarity)
df_sentiment['tb_subjectivity'] = df_sentiment['clean text'].apply(subjectivity)

# %% [markdown]
# https://huggingface.co/cardiffnlp/twitter-roberta-base-sentiment-latest

# %%
# ROBERTA polarity

# Preprocess text (username and link placeholders)
def preprocess(text):
    new_text = []
    for t in text.split(" "):
        t = '@user' if t.startswith('@') and len(t) > 1 else t
        t = 'http' if t.startswith('http') else t
        new_text.append(t)
    return " ".join(new_text)

MODEL = f"cardiffnlp/twitter-roberta-base-sentiment-latest"
tokenizer = AutoTokenizer.from_pretrained(MODEL)
config = AutoConfig.from_pretrained(MODEL)
# PT
model = AutoModelForSequenceClassification.from_pretrained(MODEL)
#model.save_pretrained(MODEL)

text = df_sentiment['clean text']

# Creates a new column in df_sentiment with text pre-processed in the way needed for this model
for i, row in df_sentiment.iterrows():
    df_sentiment.at[i, 'preprocessed text'] = preprocess(row['clean text'])

# Turn pre-processed text into list to feed into model, and make id's into list so we can pair them with
# the outcome of the model and then do a join on id with the overall dataframe
input = df_sentiment['preprocessed text'].tolist()
id_array = df_sentiment['id'].tolist()

# %%
batch_size = 100
neg_scores = []
neut_scores = []
pos_scores = []

for i in range(0, len(input), batch_size):
    current_batch = input[i: i+batch_size]
    encoded_input = tokenizer(current_batch, return_tensors='pt', padding=True, truncation=True)
    output = model(**encoded_input)
    scores = output[0].detach().numpy()
    
    for j in range (0, len(scores), 1):
        # Add scores for this batch to the overall arrays
        scores[j] = softmax(scores[j])
        neg_scores.append(scores[j][0])
        neut_scores.append(scores[j][1])
        pos_scores.append(scores[j][2])

# Create a dataframe with id_array and neg, neut, and pos scores arrays
temp = pd.DataFrame({'id': id_array, 'rbta_negative': neg_scores, 'rbta_neutral': neut_scores, 'rbta_positive': pos_scores})

# Join this dataframe with the original dataframe based on id
df_sentiment = pd.merge(df_sentiment, temp, on='id')
df_sentiment = df_sentiment.drop('preprocessed text', axis=1)

# Add column that calculates the compound polarity for roberta
df_sentiment['rbta_compound'] = (df_sentiment['rbta_positive'] - df_sentiment['rbta_negative']) * (1 - df_sentiment['rbta_neutral'])

# %% [markdown]
# ## Emotion

# %% [markdown]
# https://huggingface.co/j-hartmann/emotion-english-distilroberta-base

# %%
input = df_sentiment['clean text'].tolist()

# %%
anger = []
disgust = []
fear = []
joy = []
neutral = []
sadness = []
surprise = []

classifier = pipeline("text-classification", model="j-hartmann/emotion-english-distilroberta-base", return_all_scores=True)

for i in range(0, len(input), batch_size):
    current_batch = input[i: i+batch_size]
    output = classifier(current_batch)

    for row in output:
        anger.append(row[0]['score'])
        disgust.append(row[1]['score'])
        fear.append(row[2]['score'])
        joy.append(row[3]['score'])
        neutral.append(row[4]['score'])
        sadness.append(row[5]['score'])
        surprise.append(row[6]['score'])

#classifier = pipeline("text-classification", model="j-hartmann/emotion-english-distilroberta-base", return_all_scores=True)
#output = classifier(input)

temp = pd.DataFrame({'id': id_array, 'anger': anger, 'disgust': disgust, 'fear': fear, 'joy': joy, 'neutral': neutral, 'sadness': sadness, 'surprise': surprise})
df_sentiment = pd.merge(df_sentiment, temp, on='id')
df_sentiment['top emotion'] = df_sentiment[['anger', 'disgust', 'fear', 'joy', 'neutral', 'sadness', 'surprise']].apply(lambda row: row.idxmax(), axis=1)

# %%
# This is a different emotion model that we are not sure if we will use or not

# list_emotions = ['anger', 'anticipation', 'disgust', 'fear', 'joy', 'love', 'optimism', 'pessimism', 'sadness', 'surprise', 'trust']
# df_sentiment = df_sentiment.reindex(columns=df_sentiment.columns.tolist() + list_emotions)

# pipe = pipeline("text-classification", model="cardiffnlp/twitter-roberta-base-emotion-multilabel-latest", return_all_scores=True)

# for tweet in input:
#     output = pipe(tweet)

#     for row in output:
#         for emotion_score in row:
#             emotion = emotion_score['label']
#             score = emotion_score['score']
#             df_sentiment[emotion] = score
# df_sentiment['top emotion'] = df_sentiment[list_emotions].apply(lambda row: row.idxmax(), axis=1)

# %%
# Drop columns we dont want in final dataset
df_sentiment = df_sentiment.drop(['text', 'clean text'], axis=1)

# %%
# Save sentiment dataframe
config = yaml.safe_load(open(f"../config/config.yaml"))
sentiment_filename = config['datasets']['sentiment_filename']
sentiment_file = f"{sentiment_filename}_{sample_size}.pkl"
sentiment_data_location = os.path.join(current_directory, data_folder, sentiment_file)
df_sentiment.to_pickle(sentiment_data_location)


