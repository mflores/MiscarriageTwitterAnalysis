import json
import pandas as pd
import sys
import yaml
import os
from bertopic import BERTopic

config = yaml.safe_load(open(f"../config/config.yaml"))

current_directory = '../'

sample_size = config['datasets']['sample_size']
data_folder = config['datasets']['pickle_folder']
data_filename = config['datasets']['tweets_filename']
data = f"{data_filename}_{sample_size}.pkl"
data_location = os.path.join(current_directory, data_folder, data)

df = pd.read_pickle(data_location)
docs = df['cleaned_text'].tolist()

num_topics = config['bertopic']['num_topics']
min_df = config['bertopic']['min_df']

topics_data_filename = config['datasets']['topics_filename']
topics_data = f"{topics_data_filename}_{num_topics}_{sample_size}.pkl"
topics_data_location = os.path.join(current_directory, data_folder, topics_data)
df_topics = pd.read_pickle(topics_data_location)

model = config['bertopic']['bertopic_model']
model_name = f"{model}_{num_topics}"
#model_name = 'bertopic_model' # this is the default model
model_folder = config['bertopic']['bertopic_folder']
model_location = os.path.join(current_directory, model_folder, model_name)
topic_model = BERTopic.load(model_location)

# generate top n topics spreadsheet
grouped = df_topics.groupby(['Topic', 'Label']).size().reset_index(name='Count').sort_values(by='Count', ascending=False)
grouped = grouped.drop(0, axis=0)

top_n_topics = grouped.head(10)
df_id = df[['id', 'cleaned_text']]
df_topics_id = pd.merge(df_topics, df_id, on='id')

my_dict = {
    "topic": [],
    "label": [],
    "tweets": []
}

temp = topic_model.get_topic_info()
temp = temp.drop('Count', axis=1)

topics = top_n_topics['Topic'].tolist()
topic_labels = top_n_topics['Label'].tolist()

for i in range(0, len(topics), 1):
    top_n_topic = df_topics_id[df_topics_id['Topic'] == topics[i]]
    top_n_rows = top_n_topic.nlargest(10, 'Probability')
    top_n_rows_list = top_n_rows['cleaned_text'].tolist()
    my_dict['topic'].append(topics[i])
    my_dict['label'].append(topic_labels[i])
    my_dict['tweets'].append(top_n_rows_list)

json_data_list = []
for i in range(len(my_dict["topic"])):
    json_item = {
        "topic": my_dict["topic"][i],
        "label": my_dict["label"][i],
        "tweets": my_dict["tweets"][i]
    }
    json_data_list.append(json_item) 

filename = "llm_label_data_10.json"
with open(filename, "w") as json_file:
    json.dump(json_data_list, json_file, indent=4)

json_df = pd.DataFrame(json_data_list)
json_df.to_csv("llm_label_data_10.csv")
